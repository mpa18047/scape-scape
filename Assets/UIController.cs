﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//tell the system we are going to be using UI controls
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    //Define what image we are dealing with
    public Image Img1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //checks if the user presses q 
        if(Input.GetKeyDown("q"))
        {
            //disables the image
            Img1.enabled = false;
        }
    }
}
