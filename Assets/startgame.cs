﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class startgame : MonoBehaviour
{

    void Update()
    {
        if (StartGameCanvas.enabled == true)
        {
            Cursor.lockState = CursorLockMode.Confined;
        }
    }
    //declare the canvas
    public Canvas StartGameCanvas;

    public void StartGame()
    {
        //hide the canvas
        StartGameCanvas.enabled = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
    }
    public void HideCanvas()
    {

    }
}