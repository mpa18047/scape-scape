﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowPuzzle : MonoBehaviour
{
    //canvas
    public Canvas EPromptCanvas;
    public Canvas PuzzleCanvas;
    //text
    public Text UserInputText;
    public string SecretCode = "Green";
    //character
    public CharacterController PlayerController;

    //secret code is not yet entered 
    public bool SecretCodeEntered = false;
    public void Update()
    {
        if(UserInputText.text == SecretCode && SecretCodeEntered==false)
        {
            Debug.Log("The Secret Codde is Correct!");
            SecretCodeEntered = true;
            EPromptCanvas.enabled = false;
            PuzzleCanvas.enabled = false;
        }
    }

    void OnTriggerStay(Collider TheThingInsideTheTrigger)
    {
        if(TheThingInsideTheTrigger.tag == "Player" && SecretCodeEntered == false)
        {
            //check if they press E
            if(Input.GetKeyDown(KeyCode.E))
            {
                //show puzzle canvas
                PuzzleCanvas.enabled = true;
                //hide E prompt
                EPromptCanvas.enabled = false;
                //stop the player moving
                PlayerController.enabled = false;
                //give the player acess to the cursor
                Cursor.lockState = CursorLockMode.None;
            }
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                ExitButton();
            }
        }
    }

    public void ExitButton()
    {
        PuzzleCanvas.enabled = false;
        EPromptCanvas.enabled = true;
        //enable player movement
        PlayerController.enabled = true;
        //lock the cursor
        Cursor.lockState = CursorLockMode.Locked;
    }
}
