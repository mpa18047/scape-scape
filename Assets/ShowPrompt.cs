﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowPrompt : MonoBehaviour
{
    //the canvas that says 'Press E'
    public Canvas EPromptCanvas;

    //a reference to show puzzle script
    public ShowPuzzle PuzzleScript;

    void OnTriggerEnter(Collider TheThingEnteringTheTrigger)
    {
        if(TheThingEnteringTheTrigger.tag == "Player"&& PuzzleScript.SecretCodeEntered == false)
        {
            Debug.Log("Player is by the door");
            //show the E Prompt Canvas 
            EPromptCanvas.enabled = true;
        }
    }

    void OnTriggerExit(Collider TheThingLeaving)
    {
        if(TheThingLeaving.tag == "Player" && PuzzleScript.SecretCodeEntered == false)
        {
            Debug.Log("The play has left the door");
            //hide E Prompt cancas
            EPromptCanvas.enabled = false;
        }
    }
}
