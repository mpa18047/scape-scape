﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShowUI : MonoBehaviour
{
    

    // Update is called once per frame
    void Update()
    {
        
    }
    public Canvas EPrompt;
    public Canvas LaptopPuzzle;
    void OnTriggerEnter(Collider TheThingThatWalkedIntoMe)
    {
        if (TheThingThatWalkedIntoMe.tag == "Player")
        {
            //show the canvas
            EPrompt.enabled = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
    void OnTriggerExit(Collider TheThingThatWalkedIntoMe)
    {
        if (TheThingThatWalkedIntoMe.tag == "Player")
        {
            //show the canvas
            EPrompt.enabled = false;
            LaptopPuzzle.enabled = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
    void OnTriggerStay(Collider TheThingThatWalkedIntoMe)
    {
        if (TheThingThatWalkedIntoMe.tag == "Player")
        {
            //show the puzzle canvas when we press E
            if (Input.GetKeyDown(KeyCode.E))
            {
                LaptopPuzzle.enabled = true;
                EPrompt.enabled = false;
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                LaptopPuzzle.enabled = false;
                EPrompt.enabled = true;
            }
        }
    }
}
